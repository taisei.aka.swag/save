import { createApp } from 'vue'

//計算 読み込み
import App from './App.vue'

//css 読み込み
import text from './text.vue'

//マウント
createApp(App).mount('#app')
createApp(text).mount('#text')

import Vue from 'vue'
import 'bootstrap/dist/css/bootstrap.css' // add
import 'bootstrap-vue/dist/bootstrap-vue.css' // add
Vue.config.productionTip = false